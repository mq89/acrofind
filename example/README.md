# Acrofind Minimal Working Example

Running `acrofind.py -i` in this folder yields:

```
$ acrofind.py -i
content.tex:4 IT
\item[MATCH] This line should be found: IT

content.tex:6
IGNORE: \item[IGNORED] You might not want to use the acronym in the term IT security.

content.tex:7
IGNORE: \item[IGNORED] You can use regex to ignore lines: IT 1, IT 2, IT 3.

content.tex:10 OT
\item like this: ot [MATCH]

content.tex:11 OT
\item this: OT [MATCH]

content.tex:12 OT
\item and also like this: oT, Ot [MATCH]

content.tex:14
IGNORE: \item[IGNORED] Or maybe you don't want OT to be found?

content.tex:17 DHCP
dhcp at the beginning of a line should be found [MATCH].

content.tex:18 DHCP
A well-known DHCP server is isc-dhcp-server [MATCH] (DCHP should be found despite isc-dhcp-server is an ignore pattern).

Found 6 unwrapped acronyms.

Unused ignore patterns:
  - OT: ['this pattern never matches']
```
